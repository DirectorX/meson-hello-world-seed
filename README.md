Simple Meson C project seed
===========================

## What included?

- Very simple Make file that can
    + Trigger meson configure
    + Trigger ninja build
    + execute the binary
- Basic meson configuration
- Hello world C program