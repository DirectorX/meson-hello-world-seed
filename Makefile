build: generate
	ninja -C build

# If the first argument is "run"...
ifeq (run,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif

run: build
	build/src/hello $(RUN_ARGS)

generate: mkdir-build
	meson build

generate-debug: mkdir-build
	meson --buildtype=debug -Db_pgo=generate build

mkdir-build:
	mkdir -p build

clean:
	rm -rf build

re-generate: clean generate

re-generate-debug: clean generate-debug

.PHONY: build
